angular.module('nir.services', ['ngCordova'])

.factory('informationService', function($http) {
    var contentsList = [];

    return {
        all: function() {
            if (contentsList.length == 0){
                //return $http.get('http://192.168.0.106:4730/api/projects').then(function (response) {
                return $http.get('appdata/contents.json').then(function (response) {
                    // Json loaded on service list
                    //contentsList = response.data;
                    contentsList = response.data["contents"];
                    return contentsList;
                });
            };

            return contentsList;


        },

        get: function(contentId) {
          for (var i = 0; i < contentsList.length; i++) {
            if (contentsList[i].tag === contentId) {
                console.log(i);
                return contentsList[i];
            }
          }
          return null;
        },
    };
});

