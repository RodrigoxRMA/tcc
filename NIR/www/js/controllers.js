angular.module('nir.controllers', ['ionic'])

.controller('AppCtrl', function($scope,$rootScope, $timeout, informationService,$stateParams,$q) {

    // With the new view caching in Ionic, Controllers are only called
    // when they are recreated or on app start, instead of every page change.
    // To listen for when this page is active (for example, to refresh data),
    // listen for the $ionicView.enter event:
    //$scope.$on('$ionicView.enter', function(e) {
    //});

    informationService.all().then(function(response){
        $rootScope.contents = response;
    },function(error){
        console.log("Error on response " + response)
    });


})

.controller('ContentsCtrl', function($rootScope, informationService, ionicMaterialInk, ionicMaterialMotion) {
    // The json data will now be in scope.
    ionicMaterialInk.displayEffect();
    ionicMaterialMotion.ripple()
    //informationService.all().then(function(response){
        //$rootScope.contents = response;
        //$rootScope.apply();
    //},function(error){
        //console.log("Error on response " + response)
    //});




})

.controller('BrowseCtrl', function($scope,$rootScope,informationService,$ionicModal, ionicMaterialInk, ionicMaterialMotion ) {

    ionicMaterialInk.displayEffect();
    ionicMaterialMotion.ripple()
    // The json data will now be in scope.
    //var macAddress= "20:15:04:10:13:40";
    //var macAddress= "00:11:12:06:03:59";
    $scope.macAddress= "98:D3:31:B1:D1:22";

    $scope.pairBT = function () {

        bluetoothSerial.enable(function() {
            console.log("Bluetooth is enabled");
            $scope.info = "Bluetooth is enabled";
            $scope.$apply();
        },
        function() {

            console.log("The user did *not* enable Bluetooth");
            $scope.info = "The user did *not* enable Bluetooth";
            $scope.$apply();


        });


        bluetoothSerial.connect($scope.macAddress,function() {

            console.log("Connected to device");
            $scope.info = "Connected to device";
            $scope.$apply();
        },
        function() {

            console.log("Error connecting to device");
            $scope.info = "Error connecting to device";
            $scope.$apply();

        });

        bluetoothSerial.subscribe('\r',function(data) {

            $rootScope.data = data.substring(1).replace(/\s+/g, '');;

            $rootScope.callModal();

            //$rootScope.$apply();

            bluetoothSerial.clear();

        },
        function(data) {

            console.log("Error Subscribing");
            $scope.info = "Error Subscribing";

        });

    };


    $scope.getBT = function() {
            bluetoothSerial.read(function(data) {
                console.log(data);
                $scope.data = data.substring(1);
                $scope.$apply();
            },function(data) {
                console.log("Error Reading");
                $scope.info = "Error Reading";

            });

    };


    $rootScope.callModal = function(){
        $rootScope.content = informationService.get($rootScope.data);
        console.log($rootScope.data);
        $rootScope.modal.show();
    };

    $ionicModal.fromTemplateUrl('templates/project.html', {scope: $rootScope})
        .then(function(modal) {
            $rootScope.modal = modal;
        });

    // Triggered in the modal to close it
    $rootScope.closeModal = function() {
        $rootScope.modal.hide();
    };

    // Open the login modal
    $rootScope.project = function() {
        $rootScope.modal.show();
    };

})

.controller('ContentCtrl', function($scope, $rootScope, $stateParams, informationService, $compile) {
    $rootScope.data = $stateParams.contentId;
    $rootScope.content = informationService.get($stateParams.contentId);
    //$scope.name = $scope.localContent.name;
    //$scope.description = $scope.content.description;

})
